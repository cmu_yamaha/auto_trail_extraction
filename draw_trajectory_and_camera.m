load gt_in_cam.mat;
% x = single(x);
% y = single(y);
% z = single(z);

% subplot(2, 1, 1);
plot3(x,y,z);
grid on;
axis equal;

xlabel('x'); ylabel('y'); zlabel('z');
text(x(1), y(1), z(1), 'start');
text(x(end), y(end), z(end), 'end');

hold on;
plot3(x(1), y(1), z(1), 'rx');
plot3(x(end), y(end), z(end), 'rx');

% Draw axis
L = 10;
plot3([0, L], [0, 0], [0, 0], 'r');
plot3([0, 0], [0, L], [0, 0], 'r');
plot3([0, 0], [0, 0], [0, L], 'r');
% xlim([min(min(x), 0), max(max(x), L)])
% ylim([min(min(y), 0), max(max(y), L)])
% zlim([min(min(z), -2), max(max(z), L)])
axis auto

text(L, 0, 0, '+x');
text(0, L, 0, '+y');
text(0, 0, L, '+z');

cx = 508.5;
cy = 285.5;
fx = 469.1630859375;
fy = 469.1630859375;

u = -x ./ z * fx; % + cx;
v = -y ./ z * fy; % + cy;
u(z == 0) = 0;
v(z == 0) = 0;
ratio = 0.005;

F = fx * ratio;
plot3(u*ratio, v*ratio, -F * ones(1, numel(u)), 'b');

plot3(-u*ratio, -v*ratio, -F * ones(1, numel(u)), 'g');

% plot3(([0, 1024, 1024, 0, 0] - cx) * ratio, ([0, 0, 544, 544, 0] - cy) * ratio, -ones(1, 5), 'r--', 'FaceColor', 'red');
patch(([0, 1024, 1024, 0] - cx) * ratio, ([0, 0, 544, 544] - cy) * ratio, -F * ones(1, 4), [0, 0, 0], 'FaceAlpha', 0.1)

for i = 1:100:numel(u)
   plot3([x(i), u(i)*ratio], [y(i), v(i)*ratio], [z(i), -F], 'k--');
   plot3([x(i), u(i)*ratio], [y(i), v(i)*ratio], [z(i), -F], 'r.');
end

hold off;
view(-4.5, -66)
% subplot(2, 1, 2);
% plot(u, v);
% xlim([0, 1024]);
% ylim([0, 544]);