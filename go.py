#!/usr/bin/python

import argparse
import numpy as np
from path import Path
import sys
import math
import rosbag
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt
from scipy import interpolate
from sensor_msgs.point_cloud2 import PointCloud2
import scipy.io

from ros import tf
tf = tf.transformations

parser = argparse.ArgumentParser()

parser.add_argument(
    'bag_fn', type=Path, help='path to bag file', nargs='?', const=1,
    # default="/Data2/Research/Yamaha/data/20151216-Demo2016DataCollection/take3_area3/area3_2015-12-16-11-57-37_6.bag"
    default="/Data2/Research/Yamaha/data/20151216-Demo2016DataCollection/take3_area3/area3_2015-12-16-11-57-47_7.bag"
)

args = parser.parse_args()

cfg = {
    'imar_height': 1.70, # (unit: meter)
    'camera': {
        # w.r.t. iMar (in homogenous coor)
        'position': [0, 0.30, 0.03, 1],

        # (unit: degree) w.r.t. iMar
        'xangle': -(90 + 13) * np.pi / 180,

        # parmas: (cx, cy, fx, fy)
        'params': (508.5, 285.5, 469.1630859375, 469.1630859375)
    },
    'vehicle_width': 1.4478 # (unit: meter)
}

def pause():
    raw_input("Press Enter to continue")

def get_T_imar_utms(bag, trajectory, Ts=None):

    T_imar_utms = []

    i=0
    for topic, msg, t in bag.read_messages(['/odom']):

        # Collect imar frame over time
        [p, q] = get_pose(msg)

        if Ts is not None:
            p = trajectory[i, :]

        T_imar_utms.append(get_T_imar_utm(p, q))
        i += 1

    return np.array(T_imar_utms)

def get_trajectory(bag):

    trajectory = []

    for topic, msg, t in bag.read_messages(['/odom']):

        # Collect trajectory over time
        [p, q] = get_pose(msg)
        trajectory.append(p)

    return np.array(trajectory)

def get_T_imar_utm(p, q):
    # the order of arguments to quaternion_matrix is (x, y, z, w)
    # (see /opt/ros/indigo/lib/python2.7/dist-packages/tf/transformations.py)
    T_utm_imar = tf.quaternion_matrix((q[0], q[1], q[2], q[3]))
    T_utm_imar[:, 3] = p
    # return T_utm_imar
    return tf.inverse_matrix(T_utm_imar)

def get_T_camera_imar():
    T = tf.rotation_matrix(cfg['camera']['xangle'], (1, 0, 0))
    T[:, 3] = cfg['camera']['position']

    T = tf.inverse_matrix(T)
    return T

T_camera_imar = get_T_camera_imar()

def get_xyz_in_camera_frame(T_imar_utm, trajectory_utm):
    # xyz_i means (x, y, z) in iMar frame
    xyz_i = np.dot(T_imar_utm, trajectory_utm.T).T
    xyz_i[:, 2] -= cfg['imar_height']
    # scipy.io.savemat('gt_in_imar.mat', dict(x=xyz_i[:, 0], y=xyz_i[:, 1], z=xyz_i[:, 2]))

    # xyz_c means (x, y, z) in camera frame
    xyz_c = np.dot(T_camera_imar, xyz_i.T).T

    x = xyz_c[:, 0]
    y = xyz_c[:, 1]
    z = xyz_c[:, 2]
    # scipy.io.savemat('gt_in_cam.mat', dict(x=x, y=y, z=z))
    
    # depth (z-axis) should > 0 because z=0 is the camera plane
    valid = xyz_c[:, 2] > 0
    xyz_c = xyz_c[valid, :]

    return xyz_c

(cx, cy, fx, fy) = cfg['camera']['params']

def get_valid_uv_on_image(xyz):
    u = xyz[:, 0] / xyz[:, 2] * fx + cx # u is x (toward right in image, i.e. > )
    v = xyz[:, 1] / xyz[:, 2] * fy + cy # v is y (toward down  in image, i.e. v )

    return np.array([u, v]).T.astype(int)

def project_to_camera(trajectory, T_imar_utms, images):

    plot = None

    # trajectory of left/right wheel of vehicle in camera frame
    tL = []
    tR = []

    t = 0
    ratio = 903.0 / 257
    for tt, T_imar_utm in enumerate(T_imar_utms):

        if int(tt / ratio) - t < 1:
            continue

        t = int(tt / ratio)

        xyz_c = get_xyz_in_camera_frame(T_imar_utm, trajectory)

        L = xyz_c.copy()
        R = xyz_c.copy()

        L[:, 0] -= 0.5 * cfg['vehicle_width']
        R[:, 0] += 0.5 * cfg['vehicle_width']

        uv_L = get_valid_uv_on_image(L)
        uv_R = get_valid_uv_on_image(R)

        if t >= len(images):
            break

        img = images[t]
        draw = ImageDraw.Draw(img, 'RGBA')

        polygon = map(tuple, np.concatenate((uv_L, uv_R[::-1, :])))
        if len(polygon) >= 2:
            draw.polygon(polygon, outline=1, fill=(255, 255, 255, 128))

        if plot is None:
            plot = plt.imshow(img)
            plt.axis('off')
        else:
            plot.set_data(img)

        # img.save("images/area3_2015-12-16-11-57-37_6/{}.jpg".format(t))

        plt.pause(0.001)
        plt.draw()

    return [None, None]

# Otsu's Method for automatic binary thresholding
def OtsusBinaryThresholding(x):
    total = np.sum(x)
    s = 0
    i = 0

    maximum = 0
    threshold = None

    x = np.sort(np.reshape(x, (x.size))).astype(float)
    unique_x = np.unique(x)
    threholds = (unique_x[0:-1] + unique_x[1:]) / 2

    for t in threholds:
        while i < x.size and x[i] < t:
            s += x[i]
            i += 1
        
        obj = i * (x.size - i) * ( (s / i) - (total - s) / (x.size - i) ) ** 2

        if obj >= maximum:
            threshold = t
            maximum = obj

    return threshold

def draw_on_image(img, x, y, color=(255, 0, 0)):
    radius = 2
    r = np.arange(-radius, radius+1)
    for xx in (r + x).clip(0, 1023):
        for yy in (r + y).clip(0, 543):
            img.putpixel((xx, yy), color)

def get_pose(msg):
    pose = msg.pose.pose
    return [get_xyz_point(pose), get_quaternion(pose)]

def get_xyz_point(pose):
    p = pose.position
    return [p.x, p.y, p.z, 1]

def get_quaternion(pose):
    o = pose.orientation
    return [o.x, o.y, o.z, o.w]

def get_images(bag):

    images = []
    t_start = None
    t_end = None
    for topic, msg, t in bag.read_messages(['/multisense/left/image_rect_color']):
        if t_start == None:
            t_start = t
        t_end = t

        x = np.fromstring(msg.data, dtype=np.uint8).reshape((msg.height, msg.width, 3))
        x = Image.fromarray(x)

        b, g, r = x.split()
        x = Image.merge("RGB", (r, g, b))
        images.append(x)

    return images

def glitch_calibration(trajectory):

    # Array of transformation
    Ts = []

    points = trajectory.copy()

    def length(v):
        return np.sqrt(np.dot(v, v))

    # Compute transformation matrix from vector v1 to vector v2 in R^3
    def get_transformation_matrix(v1, v2):

        v1 = v1.copy()[0:3].astype(float)
        v2 = v2.copy()[0:3].astype(float)

        L1 = length(v1)
        L2 = length(v2)
        
        # 1) rotation axis
        axis = np.cross(v1, v2)
        axis /= length(axis)

        # 2) rotation angle
        theta = math.acos(np.dot(v1, v2) / (L1 * L2))

        # 3) scaling
        ratio = L2 / L1

        T = ratio * tf.rotation_matrix(theta, axis)
        T[3, 3] = 1

        return T

    dist = np.sqrt(np.sum(np.diff(points, axis=0) ** 2, axis=1))

    threshold = OtsusBinaryThresholding(dist)
    print threshold

    keypoint_indices = np.nonzero(dist > threshold)[0] + 1
    keypoint_indices = np.insert(keypoint_indices, 0, 0)
    print keypoint_indices
    
    prev = 0
    for i in keypoint_indices[1:]:

        # Pivot is the previous key point
        pivot = points[prev, :]

        # There's a big glitch between point[i-1] and point[i]
        v1 = points[i-1, :] - pivot;

        # Assume the points[i] is the midpoint of true points[i-1] and points[i+1]
        # i.e. points[i] = (points[i-1] + points[i+1]) / 2
        # Therefore, points[i-1] = 2 * points[i] - points[i+1]
        v2 = (2 * points[i, :] - points[i+1, :]) - pivot;

        # Find 3D transformation matrix that transform vector v1 to vector v2
        T = get_transformation_matrix(v1, v2)
        Ts.append((T, (prev+1, i)))

        # Points lie between previous keypoint and this keypoint
        points[prev+1:i, :] = np.dot(points[prev+1:i, :] - pivot, T.T) + pivot

        prev = i

    return [Ts, points]

def cubic_spline_in_3D(trajectory):
    # Number of points after spline
    N = 2000

    # trajectory CANNOT have duplicate value, otherwise it'll crash and show
    # "SystemError: error return without exception set"
    tck, u = interpolate.splprep(trajectory.T)
    new_trajectory = np.array(interpolate.splev(np.linspace(0, 1, N), tck)).T

    return new_trajectory

if __name__ == '__main__':

    # bag_filenames = [line.rstrip('\n') for line in open('all.bag.scp')]

    # for bag_fn in bag_filenames:
    bag = rosbag.Bag(args.bag_fn, 'r')
    images = get_images(bag)
    trajectory = get_trajectory(bag)
    # scipy.io.savemat('trajectory.mat', dict(trajectory=trajectory))

    # Remove glitch due to iMAR calibration failure
    [Ts, trajectory] = glitch_calibration(trajectory)
    T_imar_utms = get_T_imar_utms(bag, trajectory, Ts)

    # Smooth by cubic-spline
    trajectory = cubic_spline_in_3D(trajectory)

    [tL, tR] = project_to_camera(trajectory, T_imar_utms, images)
