function [ ] = draw_axis( )
%DRAW_AXIS Summary of this function goes here
%   Detailed explanation goes here
% Draw axis
hold on;
L = 10;
plot3([0, L], [0, 0], [0, 0], 'r');
plot3([0, 0], [0, L], [0, 0], 'r');
plot3([0, 0], [0, 0], [0, L], 'r');

text(L, 0, 0, '+x');
text(0, L, 0, '+y');
text(0, 0, L, '+z');

axis auto;
hold off;

end

