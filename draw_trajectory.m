function [] = draw_trajectory(mat_fn)
load(mat_fn);

plot3(x,y,z);
draw_axis()

grid on;
axis equal;

xlabel('x'); ylabel('y'); zlabel('z');