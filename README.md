# Auto Trail Extraction #

This repository will automatically extract trail from ROS bag. 

### What is this repository for? ###

* You need a ROS bag with the following topics:
    1. /odom (from iMAR)
    2. /multisense/left/image_rect_color (from MultiSense)
    3. /multisense/left/image_rect_color/camera_info

* This repository will use **pose (position + orientation)** from /odom to generate a **vehicle trajectory** in UTM frame, transform it from UTM frame to iMAR frame and then to camera frame, **project it onto image plane** using pinhole camera model, and then **paste it on color image**.

### Who do I talk to? ###

* Repo owner