% some data 
% ...the image 

%% Load image
% img = imread('image_rect_color.png');
m=load('clown'); 
img=flipud(m.X);

%% Load trajectory
% ...the x/y/z 
load t_in_cam.mat
x = x * 10;
y = y * 10;
z = z;
% x=1:size(img,2);
% y=100*ones(size(x));
% z=sind(x);

%% Plot image as 2D plane in 3D space
% the plot 
pimg=zeros(size(img)+[1,1]); 
pimg(1:end-1,1:end-1)=img; 
ph=pcolor(pimg); 
shading flat; % <- or shading interp

%% Plot the trajectory in 3D space
line(x,y,z,... 
 'marker','.',... 
 'markerfacecolor',[0,0,1],... 
 'linestyle','none',... 
 'color',[1,0,0]); 
% alpha(ph,.8);
% view(30,30);
% xlim([min(x), max(x)]);
% ylim([min(y), max(y)]);
% zlim([min(z), max(z)]);
xlabel('x'); ylabel('y'); zlabel('z');